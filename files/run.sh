#!/bin/bash

# Change the UID if needed

if [ ! "$(id -u mc-server)" -eq "$UID" ]; then 

	echo "Changing steam uid to $UID."

	usermod -o -u "$UID" mc-server ; 

fi

# Change gid if needed

if [ ! "$(id -g mc-server)" -eq "$GID" ]; then 

	echo "Changing steam gid to $GID."

	groupmod -o -g "$GID" mc-server ; 

fi





# Put steam owner of directories (if the uid changed, then it's needed)

chown -R mc-server:mc-server /mc-server /home/mc-server



# Launch run.sh with user steam (-p allow to keep env variables)

exec sudo -i -u mc-server /bin/bash - << %EOF%

if [ ${BUNGEECORDVERSION} = "latest" ]; then
	wget -O /mc-server/BungeeCord.jar https://ci.md-5.net/job/BungeeCord/lastSuccessfulBuild/artifact/bootstrap/target/BungeeCord.jar
else
	wget -O /mc-server/BungeeCord.jar https://ci.md-5.net/job/BungeeCord/${BUNGEECORDVERSION}/artifact/bootstrap/target/BungeeCord.jar
fi

%EOF%
