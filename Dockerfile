# Dockerfile Reference: https://docs.docker.com/engine/reference/builder/#environment-replacement
# BaseImage: Debian:Jessi
FROM openjdk:8-jdk

# ENV setttings
# Min Memory
ENV MINMEMORY 512M
# Max memory
ENV MAXMEMORY 512M
# Spigot or CraftBukkit version
ENV BUNGEECORDVERSION latest
# UID of the user mc-server
ENV UID 1000
# GID of the user mc-server
ENV GID 1000

# Install needed dependencies, create User, install dumb init (https://github.com/Yelp/dumb-init)
RUN apt-get update -y && \
    apt-get install -y wget && \
    apt-get install -y sudo && \
    apt-get clean && \
    adduser \
      --disabled-login \
      --shell /bin/bash \
      --gecos "" \
      mc-server && \
    sed -i.bkp -e \
      's/%sudo\s\+ALL=(ALL\(:ALL\)\?)\s\+ALL/%sudo ALL=NOPASSWD:ALL/g' /etc/sudoers \
      /etc/sudoers && \
    wget -O /usr/local/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.2.0/dumb-init_1.2.0_amd64 && \
    chmod +x /usr/local/bin/dumb-init

# Default structure
ADD files/run.sh /home/mc-server/run.sh
RUN usermod -a -G sudo mc-server && \
    mkdir -p /mc-server && \
    chown mc-server -R /mc-server && \
    chmod +x /home/mc-server/run.sh

# Store for all server specific data
VOLUME ["/mc-server"]


# Spigot Server reference: https://www.spigotmc.org/wiki/spigot-installation/
WORKDIR /mc-server

# Reference: https://docs.docker.com/engine/reference/builder/#entrypoint
# We need to run this script as root to probably change the UID and GID after we will switch to the mc-server user.

# Usage:
# ENTRYPOINT ["/usr/local/bin/dumb-init", "--"]
# CMD ["bash", "-c", "do-some-pre-start-thing && exec my-server"]

# We install BungeeCord during startup

ENTRYPOINT ["/usr/local/bin/dumb-init", "--"]
CMD ["bash", "-c", "/home/mc-server/run.sh && java -Xms${MINMEMORY} -Xmx${MAXMEMORY} -jar /mc-server/BungeeCord.jar nogui"]
