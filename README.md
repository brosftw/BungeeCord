# BungeeCord

## Introduction
BungeeCord is a "proxy" server software that allows for multiple Minecraft servers to be "connected" in such a way that it allows you to teleport between servers, but only use one IP to join the network of servers.

## Supported TAGS
`testtag` [Testtag on Testbranch](https://git.brosftw.ch/)  

---
## Features



---
## Usage


---
## Enviroment Variables
| ENV Variable | Default Value | Possible Values |
| ------------- |:-------------:| -----:|
| MINMEMORY | 512M | Memory in `M` or `G` |

---
## Volumes
* /buildtools: 
 * craftbukkit-X.XX.jar
 * spigot-X.XX.jar

---
## Contribution guidelines 

* Test the Containers
* Contact the [How to contribute](Link URL)
* Open a [Issue](https://git.brosftw.ch/BrosFTW/Minecraft/issues/new)

### Who do I talk to? ###

* Open an Issue @git.brosftw.ch

